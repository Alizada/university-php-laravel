<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequestStore;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $sections = (object) [
            (object) [
                'title' => "Categories",
                "cards" => Category::all(),
            ]
        ];

        return view('multiple-posts', compact('sections'));
    }

    public function indexAdmin () {
        $categories = Category::all();

        return view("admin.category", compact('categories'));
    }

    public function create()
    {
        return view('admin.category-create');
    }

    public function store(CategoryRequest $request)
    {
        $validated = $request->validated();

        if ($request->has('photo')) {
            $photoName = time() . '-' . request('photo')->getClientOriginalName();
            request('photo')->storeAs('public/category-photos', $photoName);
            $validated['photo'] = $photoName;
        }

        $category = Category::create($validated);

        return response()->redirectTo(route('admin.categories.edit', $category->slug));
    }

    public function show(Category $category)
    {
        $sections = (object) [
            (object) [
                'title' => $category->name,
                "cards" => $category->posts,
            ]
        ];

        return view('multiple-posts', compact('sections'));
    }

    public function edit(Category $category)
    {
        return view('admin.category-edit', compact('category'));
    }

    public function update(CategoryRequest $request, Category $category)
    {
        $validated = $request->validated();

        if ($request->has('photo')) {
            $photoName = time() . '-' . request('photo')->getClientOriginalName();
            request('photo')->storeAs('public/category-photos', $photoName);
            $validated['photo'] = $photoName;
        }

        $category->update($validated);

        return response()->redirectTo(route('admin.categories.edit', $category->slug));
    }

    public function destroy(Category $category)
    {
        $category->delete();

        return response()->json(["id"=>$category->id]);
    }
}
