<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class StringToSlug extends Controller
{
    public function index (Request $r) {
        return Str::slug($r->slug, '-');
    }
}
