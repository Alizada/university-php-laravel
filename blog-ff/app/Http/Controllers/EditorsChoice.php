<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class EditorsChoice extends Controller
{
    public function index()
    {
        $sections = (object) [
            (object) [
                'title' => "Editor's choice",
                "cards" => Post::where('editors_choice', true)->get(),
            ]
        ];

        return view('multiple-posts', compact('sections'));
    }
}
