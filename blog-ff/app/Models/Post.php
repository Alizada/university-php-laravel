<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Post extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function photo() {
        return $this->photo
            ? asset('storage/blog-photos/'.$this->photo )
            : ('https://dummyimage.com/300x300/000/fff&text=post');
    }

    public function hasPhoto() {
        return !!$this->photo;
    }

    public function descriptionExcerpt() {
        return Str::length($this->content) > 60 ?
            Str::substr($this->content, 0, 60)."..." :
            $this->content;
    }

    public function slug() {
        return route('posts.show', $this->slug);
    }
}
