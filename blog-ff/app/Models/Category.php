<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Category extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }

    public function photo() {
        return $this->photo
            ? asset('storage/category-photos/'.$this->photo )
            : ('https://dummyimage.com/300x300/000/fff&text=category');
    }

    public function hasPhoto() {
        return !!$this->photo;
    }

    public function descriptionExcerpt() {
        return Str::length($this->description) > 60 ?
            Str::substr($this->description, 0, 60)."..." :
            $this->description;
    }

    public function slug() {
        return route('categories.show', $this->slug);
    }

}
