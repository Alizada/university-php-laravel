@extends('layout.master')

@section('content')
    <div class="wrapper row3">
        <main class="hoc container clear">
            <div class="content">
                <div class="post-title">
                    <h1>{{ $post->title }}</h1>
                    @if($post->editors_choice)
                    <span class="editors-mark">
                        <i class="fas fa-check-circle"></i>
                        <span>Editor's choice</span>
                    </span>
                    @endif
                </div>
                <img class="imgr borderedbox inspace-5" src="{{ $post->photo() }}" style="max-width: 300px" alt="">
                <div>
                    {!! $post->content !!}
                </div>
            </div>

            <div class="clear"></div>
        </main>
    </div>
@endsection
