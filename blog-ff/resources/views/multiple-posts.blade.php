@extends('layout.master')

@section('content')
    <div class="wrapper row3">
        <main class="hoc container clear">
            <div class="content">
                <div id="gallery">
                    @foreach($sections as $section)
                        <figure>
                            <header class="heading">{{ $section->title }}</header>
                            <ul class="nospace clear latest">
                                @forelse($section->cards as $card)
                                    <article class="one_third {{($loop->index+1)%3 === 1 ?'first' : null}} mb-4">
                                        <a class="imgover d-block pos-relative" href="{{ $card->slug() }}">
                                            <img src="{{ $card->photo() }}" class="w-100" alt="">
                                            @if($card->editors_choice)
                                                <i class="fas fa-check-circle pos-absolute color-green t-5 l-5"></i>
                                            @endif
                                        </a>
                                        <div class="excerpt">
                                            <h6 class="heading h-75">{{ $card->name ? $card->name : $card->title }}</h6>
                                            <p>{!! $card->descriptionExcerpt() !!}</p>
                                            <div class="clear">
                                                <footer class="fl_right">
                                                    <a href="{{ $card->slug() }}">Read More <i class="fas fa-angle-right"></i></a>
                                                </footer>
                                            </div>
                                        </div>
                                    </article>
                                @empty
                                    <p>Nothing to show yet, check later.</p>
                                @endforelse
                            </ul>
    {{--                        <figcaption>Gallery Description Goes Here</figcaption>--}}
                        </figure>
                    @endforeach
                </div>

            </div>

            <div class="clear"></div>
        </main>
    </div>
@endsection
