<!DOCTYPE html>
<html lang="">
<head>
    <title>Fluffster</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link href="{{asset("styles/layout.css")}}" rel="stylesheet" type="text/css" media="all">
    <link rel="stylesheet" href="{{ asset("styles/mycss.css") }}">
</head>
<body id="top">

<div class="wrapper row1">
    <header id="header" class="hoc clear">
        <section style="display:flex; justify-content: center; align-items: center">
            <div style="display:flex; justify-content: center; align-items: center">
                <a href="{{route('home')}}">
                    <h1 id="logo" style="display:flex; flex-direction: column; justify-content: center; align-items: center">
                        <i class="fas fa-dove"></i> <span>Fluffster</span>
                    </h1>
                </a>
            </div>
        </section>
    </header>
</div>


<div class="wrapper row2">
    <nav id="mainav" class="hoc clear">
        <ul class="clear">
            <li class="{{request()->routeIs('home') ? "active" : null }}"><a href="{{route('home')}}">Home</a></li>
            <li class="{{request()->routeIs('categories') ? "active" : null }}"><a href="{{route('categories')}}">Categories</a></li>
            <li class="{{request()->routeIs('posts.index') ? "active" : null }}"><a href="{{route('posts.index')}}">All posts</a></li>
            <li class="{{request()->routeIs('editorsChoice') ? "active" : null }}"><a href="{{route('editorsChoice')}}">Editor's choice</a></li>
        </ul>
    </nav>
</div>

@yield('content')


<div class="wrapper row5">
    <div id="copyright" class="hoc clear">
        <!-- ################################################################################################ -->
        <p class="fl_left">Copyright &copy; 2018 - All Rights Reserved - <a href="#">Domain Name</a></p>
        <p class="fl_right">Template by <a target="_blank" href="https://www.os-templates.com/" title="Free Website Templates">OS Templates</a></p>
    </div>
</div>

<a id="backtotop" href="#top"><i class="fas fa-chevron-up"></i></a>

<script src="{{asset('scripts/jquery.min.js')}}"></script>
<script src="{{asset('scripts/jquery.backtotop.js')}}"></script>
<script src="{{asset('scripts/jquery.mobilemenu.js')}}"></script>
</body>
</html>
