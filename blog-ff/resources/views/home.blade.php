@extends('layout.master')

@section('content')
    <div id="pageintro" class="wrapper row1 bgded" style="background-image:url('{{asset("images/home-intro-back.jpg")}}');">
        <figure class="hoc clear">
            <figcaption class="heading">Late Night Show with Puppies</figcaption>
            <img src="{{ asset('images/dog-home-intro.jpg') }}" alt="">
        </figure>
    </div>


    <div class="wrapper row3">
        <main class="hoc container clear">
            <div class="sectiontitle">
                <h6 class="heading">Fabulous Dog Stories</h6>
                <p>Don't go to sleep, before you read a couple of kindest stories</p>
            </div>
            <ul id="services" class="nospace group">
                <li class="one_third">
                    <article>
                        <h6 class="heading"><i class="fas fa-bed"></i> <a href="#">Night stories for kids</a></h6>
                        <p>Make your kids animal friendly with our stories</p>
                        <footer><a href="{{ route('editorsChoice') }}">View Details <i class="fas fa-angle-right"></i></a></footer>
                    </article>
                </li>
                <li class="one_third">
                    <article>
                        <h6 class="heading"><i class="fas fa-bolt"></i> <a href="#">New stories every hour</a></h6>
                        <p>There  are plenty of stories to choose from</p>
                        <footer><a href="{{ route('posts.index') }}">View Details <i class="fas fa-angle-right"></i></a></footer>
                    </article>
                </li>
                <li class="one_third">
                    <article>
                        <h6 class="heading"><i class="fas fa-th"></i> <a href="#">Different categories</a></h6>
                        <p>Categories according to your deepest desires</p>
                        <footer><a href="{{ route('categories') }}">View Details <i class="fas fa-angle-right"></i></a></footer>
                    </article>
                </li>
            </ul>
            <footer class="center"><a class="btn" href="{{ route('posts.index') }}">All Posts <i class="fas fa-angle-right"></i></a></footer>
            <div class="clear"></div>
        </main>
    </div>


    <div class="wrapper bgded overlay" style="background-image:url('{{asset("images/demo/backgrounds/01.png")}}');">
        <section id="introblocks" class="hoc container clear">
            <div class="sectiontitle">
                <h6 class="heading">Newly added categories</h6>
                <p>Posuere arcu sed luctus nibh non viverra rutrum neque nulla pretium</p>
            </div>
            <ul class="nospace group">
                @forelse($latestCategories as $category)
                    <li class="one_third {{ $loop->first ? 'first' :  null }}">
                        <article><a href="{{ $category->slug() }}"><img src="{{ $category->photo() }}"></a>
                            <h6 class="heading">{{ $category->name }}</h6>
                            <p>{!! $category->descriptionExcerpt() !!}</p>
                            <footer><a class="btn" href="{{ $category->slug() }}">Posts <i class="fas fa-angle-right"></i></a></footer>
                        </article>
                    </li>
                @empty
                    Nothing to show yet
                @endforelse
            </ul>

            <footer class="center mt-6"><a class="btn" href="{{ route('categories') }}">View More Categories <i class="fas fa-angle-right"></i></a></footer>
        </section>
    </div>


    <div class="wrapper row3">
        <section class="hoc container clear">
            <div class="sectiontitle">
                <h6 class="heading">Latest Stories</h6>
                <p>Our editors are posting every hour, make sure you check new posts hourly</p>
            </div>

            <div id="latest" class="group">
                @forelse($latestPosts as $card)
                    <article class="one_third {{($loop->index+1)%3 === 1 ?'first' : null}} mb-4">
                        <a class="imgover d-block pos-relative" href="{{ $card->slug() }}">
                            <img src="{{ $card->photo() }}" class="w-100" alt="">
                            @if($card->editors_choice)
                                <i class="fas fa-check-circle pos-absolute color-green t-5 l-5"></i>
                            @endif
                        </a>
                        <div class="excerpt">
                            <h6 class="heading h-75">{{ $card->name ? $card->name : $card->title }}</h6>
                            <p>{!! $card->descriptionExcerpt() !!}</p>
                            <div class="clear">
                                <footer class="fl_right">
                                    <a href="{{ $card->slug() }}">Read More <i class="fas fa-angle-right"></i></a>
                                </footer>
                            </div>
                        </div>
                    </article>
                @empty
                    <p>Nothing to show yet, check later.</p>
                @endforelse
            </div>

            <footer class="center"><a class="btn" href="{{ route('posts.index') }}">View More Posts <i class="fas fa-angle-right"></i></a></footer>
        </section>
    </div>

@endsection
