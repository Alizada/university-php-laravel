<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    function index() {
        return view('home', ["students" => Student::all()]);
    }
}
